type CarYear = number;
type Cartype = string;
type CarModel = string;

type Car ={
    year: CarYear,
    type: Cartype,
    model: CarModel,
}

const carYear: CarYear = 2001;
const cartype: Cartype = "Toyota";
const carModel: CarModel = "Corolla"

const car1: Car = {
    year:2001,
    type:"Nissan",
    model:"XXX",
}
console.log(car1)