interface Rectangle {
    width: number;
    height: number;
}

interface ColoredRectangel extends Rectangle {
    color: string;
}

const rectangle: Rectangle = {
    width: 20,
    height: 10,
}

console.log(rectangle)

const coloredRectangel: ColoredRectangel = {
    width:20,
    height:10,
    color:"red"

}
console.log(coloredRectangel)